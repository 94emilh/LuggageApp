package dto;

import java.util.Date;

public class LuggageDto {
    private final int id;
    private final Date checkInDate;
    private final Date checkOutDate;
    private static int counter = 0 ;

    public LuggageDto(int id, Date checkInDate, Date checkOutDate) {
        this.id = id;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
    }

    public LuggageDto(){
        this(++counter,new Date(),new Date());
    }

    public int getId() {
        return id;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    @Override
    public String toString() {
        return "LuggageDto{" +
                "id=" + id +
                ", checkInDate=" + checkInDate +
                ", checkOutDate=" + checkOutDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LuggageDto)) return false;

        LuggageDto that = (LuggageDto) o;

        if (id != that.id) return false;
        if (checkInDate != null ? !checkInDate.equals(that.checkInDate) : that.checkInDate != null) return false;
        return checkOutDate != null ? checkOutDate.equals(that.checkOutDate) : that.checkOutDate == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (checkInDate != null ? checkInDate.hashCode() : 0);
        result = 31 * result + (checkOutDate != null ? checkOutDate.hashCode() : 0);
        return result;
    }

}
