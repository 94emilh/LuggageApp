package dto;

import java.util.ArrayList;
import java.util.List;

public class WarehouseDto {
    private final List<LuggageDto> listOfLuggages = new ArrayList<>();
    private final int totalNumberOfSlots = 100;

    public List<LuggageDto> getListOfLuggages() {
        return listOfLuggages;
    }

    public int getTotalNumberOfSlots() {
        return totalNumberOfSlots;
    }

    @Override
    public String toString() {
        return "WarehouseDto{" +
                "listOfLuggages=" + listOfLuggages +
                ", totalNumberOfSlots=" + totalNumberOfSlots +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WarehouseDto)) return false;

        WarehouseDto that = (WarehouseDto) o;

        return listOfLuggages != null ? listOfLuggages.equals(that.listOfLuggages) : that.listOfLuggages == null;
    }

    @Override
    public int hashCode() {
        return listOfLuggages != null ? listOfLuggages.hashCode() : 0;
    }
}
