import controllers.MainMenuController;

import java.text.ParseException;

public class LuggageAppMain {

    public static void main(String[] args) throws ParseException {
        MainMenuController.showMainMenu();
    }
}
