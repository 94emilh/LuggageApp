package controllers;

import dao.WarehouseDao;
import dto.LuggageDto;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

public class MainMenuController {

    public static void showMainMenu() throws ParseException {

        int numberOfAvailableSlots = WarehouseDao.getNumberOfAvailableSlots();

        while (true) {

            System.out.println("Welcome to Luggage app! Please choose one of the below options!");

            char c = TextMenuUtil.readChoice(
                    "C - check the number of available slots\n" +
                            "A - add a new luggage\n" +
                            "R - remove your luggage\n" +
                            "Q - Quit", 'C', 'A', 'R', 'Q');

            switch (c) {

                case 'C':
                    if (numberOfAvailableSlots == 0) {
                        System.err.println("Sorry, there are no more available slots at the moment, please come back later"); //in case the warehouse is full
                    } else {
                        System.out.println("There are " + WarehouseDao.getNumberOfAvailableSlots() + " slots available in the warehouse!");
                    }
                    break;

                case 'A':
                    if (WarehouseDao.getNumberOfAvailableSlots() == 0) {
                        System.out.println("Sorry, there are no more available slots at the moment, please come back later"); // in case the warehouse is full, user is not supposed to be able to add a new luggage
                    }
                    LuggageDto newLuggage = new LuggageDto();
                    WarehouseDao.insert(newLuggage);
                    System.out.println("Your luggage has been checked in - ID " + newLuggage.getId() + " on date : " + WarehouseDao.formatDate(newLuggage.getCheckInDate(), "dd/MM/yyyy HH:mm"));
                    break;

                case 'R':
                    if (WarehouseDao.getAll().size() == 0) {
                        System.err.println("There are no luggages in the warehouse, going back to main menu!");
                        break;
                    }
                    int idToDel;
                    idToDel = TextMenuUtil.readInt("Insert the id of the luggage you want to check out: "); // validating that the id from the client is an int

                    List<Integer> listWithIds = WarehouseDao.getAll().stream().
                            map(LuggageDto::getId).collect(Collectors.toList());

                    if (!listWithIds.contains(idToDel)) {    //validating that the luggage with the received ID actually exists in the warehouse
                        System.err.println("Invalid Id, going back to main menu!");
                        break;
                    }

                    String formattedCheckInDate = WarehouseDao.formatDate(WarehouseDao.get(idToDel).getCheckInDate(), "dd/MM/yyyy HH:mm");
                    String formattedCheckOutDate = WarehouseDao.formatDate(WarehouseDao.get(idToDel).getCheckOutDate(), "dd/MM/yyyy HH:mm");

                    long realDurationOfStay = WarehouseDao.computeTimeDifference(formattedCheckInDate, formattedCheckOutDate);
                    System.out.println("Summary for luggage with id " + idToDel + "\n" +
                            "Duration - " + realDurationOfStay + " minutes" +
                            " and cost - " + WarehouseDao.computeCost(realDurationOfStay) + " RON has been checked out at " +
                            formattedCheckOutDate);
                    System.out.println("\nThank you for choosing Luggage app!");
                    WarehouseDao.delete(idToDel);
                    return;

                default:
                    System.out.println("Thank you for visiting us!");
                    return; //exiting main loop
            }
        }

    }
}
