package dao;

import dto.LuggageDto;
import dto.WarehouseDto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class WarehouseDao {
    private static WarehouseDto warehouseDto = new WarehouseDto();

    /**
     * inserting a new luggage in the warehouse + generating a new ID for his/her luggage
     */

    public static void insert(LuggageDto newLuggage){
        warehouseDto.getListOfLuggages().add(newLuggage);
    }

    /**
     * getting a luggage for a certain id
     */

    public static LuggageDto get(int id){
        return warehouseDto.getListOfLuggages().get(id-1);
    }

    /**
     * getting full list of luggages currently in the warehouse (for testing purposes)
     */

    public static List<LuggageDto> getAll() {
        return warehouseDto.getListOfLuggages();
    }

    /**
     * calculating current number of available slots
     */

    public static int getNumberOfAvailableSlots(){
        return warehouseDto.getTotalNumberOfSlots() - warehouseDto.getListOfLuggages().size();
    }

    /**
     * checking-out a luggage and removing it from the warehouse
     */

    public static void delete(int id){
        warehouseDto.getListOfLuggages().remove(id-1);
    }

    /**
     * STATIC UTILITY METHODS
     */

    /**
     * method for formatting the time as per our needs
     */

    public static String formatDate(Date date, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(date);
    }

    /**
     * method for computing the exact time difference in minutes between check in and check out date
     */

    public static long computeTimeDifference(String date1, String date2) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
        Date firstDate = sdf.parse(String.valueOf(date1));
        Date secondDate = sdf.parse(String.valueOf(date2));
        long diffInMillis = Math.abs(secondDate.getTime() - firstDate.getTime());

        return TimeUnit.MINUTES.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }

    /**
     * method for computing the cost using the exact duration in minutes
     */

    public static long computeCost(long duration) {
        if (duration <= 60) {
            return 10;
        } else if(duration % 60 == 0){
            return 10 + 5 * (duration / 60 - 1);
        } else {
            return 10 + 5 * (duration / 60);
        }
    }

}
