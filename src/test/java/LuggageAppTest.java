import dao.WarehouseDao;
import dto.LuggageDto;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LuggageAppTest {

    private static final long baseTime = System.currentTimeMillis();
    private List<LuggageDto> sampleListOfLuggages;

    @Before
    public void insertLuggagesBeforeTest() {
        assertTrue(WarehouseDao.getAll().isEmpty());

        // adding some sample luggages
        WarehouseDao.insert(new LuggageDto(1, new Date(baseTime), new Date(baseTime + 1000 * 60))); // check out date - 1 min after check in
        WarehouseDao.insert(new LuggageDto(2, new Date(baseTime), new Date(baseTime + 1000 * 60 * 61))); // check out date - 1h1min after check in
        WarehouseDao.insert(new LuggageDto(3, new Date(baseTime), new Date(baseTime + 1000 * 60 * 60 * 25))); // check out date - 1d1h after check in

        sampleListOfLuggages = WarehouseDao.getAll();
    }

    @After
    public void deleteLuggagesAfterTest() {
        WarehouseDao.getAll().removeAll(WarehouseDao.getAll());
        assertTrue(WarehouseDao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        List<LuggageDto> currentListOfLuggages = WarehouseDao.getAll();
        assertEquals(3, sampleListOfLuggages.size());
    }

    @Test
    public void checkNumberOfAvailableSlotsForDefaultWarehouse() {
        assertEquals(97, WarehouseDao.getNumberOfAvailableSlots());
    }

    @Test
    public void checkNumberOfAvailableSlotsAfterInsert() {
        WarehouseDao.insert(new LuggageDto(4, new Date(baseTime), new Date(baseTime + 1000)));
        assertEquals(96, WarehouseDao.getNumberOfAvailableSlots());
    }

    @Test
    public void checkInsertMethod() {
        LuggageDto newLuggage = new LuggageDto(4, new Date(baseTime), new Date(baseTime + 3 * 60 * 1000));
        WarehouseDao.insert(newLuggage);
        assertEquals(4, WarehouseDao.getAll().size());
        assertEquals(WarehouseDao.get(4), newLuggage);
    }

    @Test
    public void checkRemoveMethodForEmptyList() {
        final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        WarehouseDao.getAll().removeAll(WarehouseDao.getAll());
        Assert.assertEquals(0, sampleListOfLuggages.size());
    }

    @Test
    public void checkRemoveMethodForAnActualLuggage() {
        WarehouseDao.delete(1);
        assertEquals(2, sampleListOfLuggages.size());
    }

    @Test
    public void checkRealDurationOfStay() throws ParseException {
        Date checkInDateForSecondLuggage = sampleListOfLuggages.get(1).getCheckInDate();
        Date checkOutDateForSecondLuggage = sampleListOfLuggages.get(1).getCheckOutDate();
        String formattedCheckInDate = WarehouseDao.formatDate(checkInDateForSecondLuggage, "dd/MM/yyyy HH:mm");
        String formattedCheckOutDate = WarehouseDao.formatDate(checkOutDateForSecondLuggage, "dd/MM/yyyy HH:mm");

        assertEquals(61, WarehouseDao.computeTimeDifference(formattedCheckInDate, formattedCheckOutDate));
    }

    @Test
    public void checkComputeCostMethod() {
        assertEquals(10, WarehouseDao.computeCost(1));
        assertEquals(15, WarehouseDao.computeCost(61));
        assertEquals(130, WarehouseDao.computeCost(1500));
    }

}
